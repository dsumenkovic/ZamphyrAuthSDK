package sumenkovic.sdk.authentication;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;


public class Authentication {

    public static String code;
    public static Token token;

    private static ChromeCustomTab chromeCustomTab;
    private static String CLIENT_ID;
    private static String CLIENT_SECRET;
    private static String REDIRECT_URI;
    private static String STATE;
    public static Context CONTEXT;


    public static void chromeCustomTabShow(){
        chromeCustomTab.show();

    }
    public static void openLoginActivity(Context context,
                                         final String clientId,
                                         final String clientSecret,
                                         final String redirectUri,
                                         final String state){
        CLIENT_ID = clientId;
        CLIENT_SECRET = clientSecret;
        REDIRECT_URI = redirectUri;
        STATE = state;
        CONTEXT = context;

        StringBuilder url = new StringBuilder();
        Formatter fmt = new Formatter(url);
        fmt.format("https://id.zamphyr.com/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=code&state=%s", clientId, redirectUri, state );
        Log.i("LINK", url.toString());
        chromeCustomTab = new ChromeCustomTab((Activity)context, url.toString());
        chromeCustomTab.warmup();
        chromeCustomTab.mayLaunch();
    }


    public static void getToken(final VolleyCallback volleyCallback){

        String url = "https://id.zamphyr.com/oauth/token";
        RequestQueue queue = Volley.newRequestQueue(CONTEXT);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.i("RESPONSE", "" + response);
                        HashMap<String,String> map = new Gson().fromJson(response, new TypeToken<HashMap<String, String>>(){}.getType());
                        token = new Token(map.get("token_type"), map.get("access_token"), map.get("expires_in"));
                        volleyCallback.onSuccess(response);

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERROR", error.toString());
                    }
                }
        ) {

            @Override
            protected Map<String,String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                params.put("client_id", CLIENT_ID);
                params.put("client_secret", CLIENT_SECRET);
                params.put("grant_type", "authorization_code");
                params.put("redirect_uri", REDIRECT_URI);
                params.put("code", code);
                return params;
            }};
        queue.add(stringRequest);
    }

    public interface VolleyCallback{
        void onSuccess(String result);
    }
}

