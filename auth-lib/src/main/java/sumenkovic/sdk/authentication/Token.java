package sumenkovic.sdk.authentication;

public class Token {

    private String tokenType;
    private String accessToken;
    private String tokenExpiration;

    public Token() {
        this.tokenType = null;
        this.accessToken = null;
        this.tokenExpiration = null;
    }

    public Token(String tokenType, String accessToken, String tokenExpiration) {
        this.tokenType = tokenType;
        this.accessToken = accessToken;
        this.tokenExpiration = tokenExpiration;
    }

    public String getTokenType() {
        return tokenType;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getTokenExpiration() {
        return tokenExpiration;
    }
}
