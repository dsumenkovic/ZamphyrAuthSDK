package sumenkovic.sdk.authentication;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AuthCallbackActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Uri data = getIntent().getData();
        Pattern pattern = Pattern.compile("code=((\\w|\\d)+)&");
        Matcher matcher = pattern.matcher(data.toString());
        matcher.find();
        Authentication.code = matcher.group(1);

        Intent intent = new Intent(this, Authentication.CONTEXT.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        Log.i("INTENT",intent.toUri(Intent.URI_INTENT_SCHEME));
        finish();
    }

}
